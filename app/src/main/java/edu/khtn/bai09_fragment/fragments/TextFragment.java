package edu.khtn.bai09_fragment.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.khtn.bai09_fragment.R;

public class TextFragment extends Fragment{
    static public String CURRENT_PATH = "";
    LinearLayout textLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view,null,false);
        textLayout = (LinearLayout)view.findViewById(R.id.imgLayout);
        File file = new File(CURRENT_PATH);
        ArrayList<String> stringList = readTextFile(file);
        for (String line : stringList) {
            createTextViewAndAddToLayout(line);
        }
        return view;
    }

    public ArrayList readTextFile(File file){
        try {
            Scanner sc = new Scanner(file);
            ArrayList<String> stringList = new ArrayList<>();
            while (sc.hasNextLine())
                stringList.add(sc.nextLine());
            return stringList;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }return  null;
    }

    public void createTextViewAndAddToLayout(String line){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView textView = new TextView(getActivity());
        textView.setLayoutParams(params);
        textView.setText(line);
        textLayout.addView(textView);
    }
}
