package edu.khtn.bai09_fragment.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;

import edu.khtn.bai09_fragment.R;

public class FilesFragment extends Fragment implements View.OnClickListener {
    static public File[][] arrFilesList = new File[10][100];
    static public String CURRENT_PATH = "";
    String[] mediaExtenList, imageExtenList, textExtenList;
    LinearLayout filesLayout;
    Button btnNewDir;
    int pathLevel = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_files, null, false);
        filesLayout = (LinearLayout) view.findViewById(R.id.filesLayout);
        btnNewDir = (Button) view.findViewById(R.id.newFolder_btn);
        btnNewDir.setOnClickListener(this);
        arrFilesList[pathLevel] = getExternalStorage();
        setNameForButtonAndAddToFileLayout();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.newFolder_btn:
                newDirectory();
                break;
        }
    }

    public void setNameForButtonAndAddToFileLayout() {
        filesLayout.removeAllViews();
        if (pathLevel > 0) {
            Button btnBack = createButton("Back");
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pathLevel--;
                    setNameForButtonAndAddToFileLayout();
                    String[] pathSplit = CURRENT_PATH.split("/");
                    CURRENT_PATH = "";
                    for (int j = 1; j < pathSplit.length - 1; j++) {
                        CURRENT_PATH += "/" + pathSplit[j];
                    }
                }
            });
            filesLayout.addView(btnBack);
        }
        for (File file : arrFilesList[pathLevel]) {
            Button button = createFilesButton(file);
            filesLayout.addView(button);
        }
    }

    public Button createFilesButton(final File file) {
        Button button = createButton(file.getName());
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (file.isDirectory()) {
                    openFolder(file);
                } else if (file.isFile()) {
                    openFiles(file);
                }
            }
        });
        return button;
    }

    public Button createButton(String name) {
        LinearLayout.LayoutParams buttonType = new LinearLayout.LayoutParams
                (300, LinearLayout.LayoutParams.WRAP_CONTENT);
        Button button = new Button(getActivity());
        button.setText(name);
        button.setLayoutParams(buttonType);
        return button;
    }

    public void openFolder(File file) {
        pathLevel++;
        CURRENT_PATH = file.getAbsolutePath();
        arrFilesList[pathLevel] = file.listFiles();
        setNameForButtonAndAddToFileLayout();
    }

    public void openFiles(File file) {
        mediaExtenList = new String[]{".mp3", ".wav", ".wma", ".3gp"};
        for (String s : mediaExtenList) {
            if (file.getName().endsWith(s)) {
                Mp3Fragment.CURRENT_PATH = CURRENT_PATH + "/" + file.getName();
                Fragment mp3Fragment = new Mp3Fragment();
                addFragmentToHomeLayout(mp3Fragment, "mp3",true);
                break;
            }
        }
        imageExtenList = new String[]{".jpg", ".png", ".bmp", ".gif", ".jpeg"};
        for (String s : imageExtenList) {
            if (file.getName().endsWith(s)) {
                ImgFragment.CURRENT_PATH = CURRENT_PATH + "/" + file.getName();
                Fragment imgFragment = new ImgFragment();
                addFragmentToHomeLayout(imgFragment, "image",true);
                break;
            }
        }
        textExtenList = new String[]{".txt", ".bin", ".bat", ".java"};
        for (String s : textExtenList) {
            if (file.getName().endsWith(s)) {
                TextFragment.CURRENT_PATH = CURRENT_PATH + "/" + file.getName();
                Fragment textFragment = new TextFragment();
                addFragmentToHomeLayout(textFragment, "text",true);
                break;
            }
        }
    }

    public void newDirectory() {
        String folderName = "New Folder";
        Button button = createButton(folderName);
        File file = new File(CURRENT_PATH, folderName);
        if (isStorageWritable()) {
            int j = 0;
            while (file.exists()) {
                j++;
                file = new File(CURRENT_PATH, folderName + j);
                button = createButton(folderName + j);
            }
            file.mkdir();
            filesLayout.addView(button);
        }
    }

    public void addFragmentToHomeLayout
            (Fragment fragment, String key, boolean addToStack) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.homeLayout, fragment);
        if (addToStack)
            transaction.addToBackStack(key);
        transaction.commit();
    }

    public File[] getExternalStorage() {
        if (isStorageReadable()) {
            File externalStorage = Environment.getExternalStorageDirectory();
            CURRENT_PATH = externalStorage.getAbsolutePath();
            return externalStorage.listFiles();
        } else
            return null;
    }

    public boolean isStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
            return true;
        return false;
    }

    public boolean isStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
            return true;
        return false;
    }

    public void toast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

    }
}
