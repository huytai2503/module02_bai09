package edu.khtn.bai09_fragment.fragments;

import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import edu.khtn.bai09_fragment.R;

public class ImgFragment extends Fragment{
    static public String CURRENT_PATH = "";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view,null,false);
        LinearLayout imgLayout = (LinearLayout) view.findViewById(R.id.imgLayout);
        ImageView imageView = new ImageView(getActivity());
        Drawable drawable = Drawable.createFromPath(CURRENT_PATH);
        imageView.setImageDrawable(drawable);
        imgLayout.addView(imageView);
        return view;
    }
}
