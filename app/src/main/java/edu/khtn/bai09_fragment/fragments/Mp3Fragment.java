package edu.khtn.bai09_fragment.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;

import edu.khtn.bai09_fragment.R;

public class Mp3Fragment extends Fragment implements
        View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    static public String CURRENT_PATH = "";
    Button btnPlay, btnPause, btnStop, btnExit;
    MediaPlayer mp;
    SeekBar seekBar;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mp3, null, false);
        linkViewAndSetOnClick();
        mp = new MediaPlayer();
        try {
            mp.reset();
            mp.setDataSource(CURRENT_PATH);
            mp.prepare();
            mp.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                exit();
            }
        });
        return view;
    }

    public void exit() {
        if (mp.isPlaying())
            mp.stop();
        Fragment fragment = new FilesFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.homeLayout, fragment);
        transaction.commit();
    }

    public void play() {
        if (mp.isPlaying() == false) {
            try {
                mp.prepare();
                mp.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void pause() {
        if (mp.isPlaying())
            mp.stop();
    }

    public void stop() {
        seekBar.setProgress(0);
        if (mp.isPlaying()) {
            mp.seekTo(0);
            mp.stop();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exit_btn:
                exit();
                break;
            case R.id.play_btn:
                play();
                break;
            case R.id.pause_btn:
                pause();
                break;
            case R.id.stop_btn:
                stop();
                break;
            default:
                break;
        }
    }

    @Override
    public void onStop() {
        exit();
        super.onStop();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
        float msPerSeekValue = mp.getDuration() / seekBar.getMax();
        int msCurrentSeek = (int) (msPerSeekValue * value);
        mp.seekTo(msCurrentSeek);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void linkViewAndSetOnClick(){
        btnPlay = (Button) view.findViewById(R.id.play_btn);
        btnPlay.setOnClickListener(this);
        btnPause = (Button) view.findViewById(R.id.pause_btn);
        btnPause.setOnClickListener(this);
        btnStop = (Button) view.findViewById(R.id.stop_btn);
        btnStop.setOnClickListener(this);
        btnExit = (Button) view.findViewById(R.id.exit_btn);
        btnExit.setOnClickListener(this);
        seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setMax(100);
    }

    public void toast(String msg) {
        Toast.makeText(getActivity().getBaseContext(),
                msg, Toast.LENGTH_SHORT).show();
    }
}
